//
//  AppDelegate.h
//  FogGuide
//
//  Created by BC Holmes on 2013-01-13.
//  Copyright (c) 2013 Ayizan Studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseAppDelegate.h"

@interface AppDelegate : BaseAppDelegate

@end
