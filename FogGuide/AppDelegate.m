//
//  AppDelegate.m
//  FogGuide
//
//  Created by BC Holmes on 2013-01-13.
//  Copyright (c) 2013 Ayizan Studios. All rights reserved.
//

#import "AppDelegate.h"
#import "BCHTheme.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.currentCon = @"fogcon2020";
    self.timeZone = [NSTimeZone timeZoneWithName:@"America/Los_Angeles"];
    self.theme = [[BCHTheme alloc] initWithBaseColor:
                  [UIColor colorWithRed:(132.0 / 255.0) green:(57.0 / 255.0) blue:(26.0 / 255.0) alpha:1]];
    self.theme.bottomBarColor = self.theme.baseColor;
    return [super application:application didFinishLaunchingWithOptions:launchOptions];
}

@end
